#!/bin/bash

#Put this directory on the stack, so that we can return a bit later.
pushd . >/dev/null

#Get into the root of the Bullet Solver project...
cd ..
#...and then save the path...
BULLET_PATH=`basename \`pwd\``
#...and then use the root directory name as the tar filename
echo "bullet path: $TARFILE"
cd ..
tar --exclude=*.o --exclude=*.swp -cvzf $BULLET_PATH.tar.gz $BULLET_PATH/*

#Pop the path from the stack
popd > /dev/null
