
HOUDINI_VERSION=10.0.390


DSONAME=SIM_SolverBullet
SOURCES= src/SIM_SolverBullet.cpp src/SIM_BulletData.cpp src/SIM_SolverBulletState.cpp src/Gdp_Condition.cpp
#Location where to Copy the DSO
LOCATION=dso/

# Bullet Include Directory
INCDIRS= -I../../bulletsrc/bullet-2.75/src -I./src
# Bullet Library Directory
LINKDIRS =  -L../../bulletSrc/bullet-2.75/src/.libs 

STATIC_LINK_FLAGS = -lbulletdynamics -lbulletcollision -lbulletmath 
DYNAMIC_LINK_FLAGS = 


UVAR := $(shell uname)

ifeq ($(OS), Windows_NT)
	ifndef OPTIMIZER
		OPTIMIZER = -Ox
	endif
	include ${HFS}/toolkit/makefiles/Makefile.win
	DSOEXT=.dll
	
	

else
	ifeq ($(UVAR), Darwin)
		#use provided mac Makefile
		include makefiles/Makefile.mac
		DSOEXT=.dylib
		
	else
		include ${HFS}/toolkit/makefiles/Makefile.linux
		DSOEXT=.so		
		LOCATIONADD=
		STATIC_LINK_FLAGS=-Wl,-Bstatic ${STATIC_LINK_FLAGS} -Wl,-Bdynamic
		


	endif
endif


DSONAME:=$(LOCATION)$(DSONAME)$(DSOEXT)

#DEBUG FLAGS
OPTIMIZER = -g -DUT_ASSERT_LEVEL=2 

#OPTIMIZER = -O2

OBJECTS = $(SOURCES:.cpp=.o)

ifdef DSONAME
TAGINFO = $(shell (echo -n "Compiled on:" `date`"\n         by:" `whoami`@`hostname`"\n$(SESI_TAGINFO)") | sesitag -m)

%.o:		%.cpp
	$(CC) $(OBJFLAGS) -g -DMAKING_DSO $(TAGINFO) $< $(OBJOUTPUT)$@

$(DSONAME):	$(OBJECTS)
	$(LINK) $(SHAREDFLAG) $(OBJECTS)  ${LINKDIRS} ${STATIC_LINK_FLAGS} ${DYNAMIC_LINK_FLAGS} $(DSOFLAGS)  $(DSOOUTPUT)$@
else
%.o:		%.cpp
	$(CC) $(OBJFLAGS) $(OPENMP_FLAGS) $< $(OBJOUTPUT)$@

$(APPNAME):	$(OBJECTS)
	$(LINK) $(OBJECTS) $(OPENMPLD_FLAGS) $(SAFLAGS) $(SAOUTPUT)$@
endif

default:	$(DSONAME) $(APPNAME)


clean:
	rm -f $(OBJECTS) $(APPNAME) $(DSONAME)


