
#include "SIM_SolverBulletState.h"


void
SIM_SolverBulletState::addReference()
{
	++refCount;
}

void
SIM_SolverBulletState::removeReference()
{
	--refCount;
	if (refCount < 1)
		delete this;
}

SIM_SolverBulletState::SIM_SolverBulletState() : refCount(0)
{
	initSystem();
}

SIM_SolverBulletState::~SIM_SolverBulletState()
{
	if( m_dynamicsWorld != NULL )
		cleanSystem();
}
