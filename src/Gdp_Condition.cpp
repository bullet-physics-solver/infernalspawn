/*
 * Gdp_Condition.cpp
 *
 *  @Created on: 14.02.2010
 *  @Author: Sebastian H. Schmidt
 */

#include "Gdp_Condition.h"

Gdp_Condition::Gdp_Condition()
{
	this->reset();
}

void Gdp_Condition::reset()
{
	this->numPrimPoly = 0;
	this->numPrimSphere = 0;
	this->numPrimMesh = 0;
	this->numPrimOther = 0;
	this->numPolyTri	= 0;
	this->numPolyOther	= 0;
}

void Gdp_Condition::check(GU_Detail *gdp)
{
	this->reset();
	GEO_PrimList primList = ((GEO_Detail*) gdp)->primitives();
	uint nprim = primList.entries();
	for (uint i=0;i < nprim;i++)
	{
		unsigned int primID = primList.entry(i)->getPrimitiveId();
		if ( primID & GEOPRIMPOLY)
		{
			this->numPrimPoly++;
			if (primList.entry(i)->getVertexCount() == 3)
			{
				this->numPolyTri++;
			}
			else
			{
				this->numPolyOther++;
			}
		}
		else if (primID & GEOPRIMSPHERE)
		{
			GEO_PrimSphere *sphere = dynamic_cast<GEO_PrimSphere*>(primList.entry(i));
			UT_Matrix3 transMat = sphere->getTransform();
			UT_Vector3 scales;
			transMat.extractScales(scales);
			if (scales.minComponent() != scales.maxComponent())
			{
				this->numSphereTransform++;
			}
			else
			{
				this->numSphereNormal++;
			}
			this->numPrimSphere++;
		}
		else if (primID & GEOPRIMMESH)
		{
			this->numPrimMesh++;
		}
		else
		{
			this->numPrimOther++;
		}
	}
}
bool Gdp_Condition::isConvexReady(bool strict)
{
	bool ret_val = false;
	ret_val = (this->numPrimMesh > 0) || (this->numPrimPoly > 0);
	if ((strict) && ( (this->numPrimSphere > 0) || (this->numPrimOther  > 0 )))
	{
		ret_val = false;
	}
	return ret_val;
}

bool Gdp_Condition::isConcaveReady(bool strict)
{
	bool ret_val = false;
	ret_val = ((this->numPrimPoly > 0) && (this->numPolyTri > 0));
	if ((strict) && ((this->numPrimOther > 0) || (this->numPrimSphere > 0) || (this->numPrimMesh > 0) || (this->numPolyOther > 0)))
	{
		ret_val = false;
	}
	return ret_val;
}

bool Gdp_Condition::isSphereTreeReady(bool strict)
{
	bool ret_val = false;
	ret_val = (this->numPrimSphere > 0);
	if ((strict) && ((this->numPrimOther > 0) || (this->numPrimMesh > 0) || (this->numPrimPoly > 0)))
	{
		ret_val = false;
	}
	return ret_val;
}

uint Gdp_Condition::getSphereCount()
{
	return this->numPrimSphere;

}
