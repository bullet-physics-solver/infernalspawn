// 
// Open Source Bullet Solver
//


#include "SIM_SolverBullet.h"



#define RAD_TO_DEG (180.0/M_PI)
#define DEG_TO_RAD (M_PI/180.0)

#define COUT_BTVEC(BVEC) BVEC.getX() << " "<<  BVEC.getY() << " " << BVEC.getZ()
#define COUT_BTRANS_ORIGIN(BTRANS) COUT_BTVEC(BTRANS.getOrigin())
#define COUT_BTRANVEC_ORIGIN(BTRANS) COUT_BTVEC(BTRANS->getOrigin().getX())


btTransform convertTobtTransform(const UT_DMatrix4 &inMat)
{
	UT_Matrix3 rotmatrix;
	inMat.extractRotate( rotmatrix );
	btMatrix3x3 btRotMatrix( rotmatrix(0,0), rotmatrix(0,1), rotmatrix(0,2),
				 rotmatrix(1,0), rotmatrix(1,1), rotmatrix(1,2),
				 rotmatrix(2,0), rotmatrix(2,1), rotmatrix(2,2) );
	UT_Vector3  translates;
	inMat.getTranslates( translates );
	btTransform btTrans;
	btTrans.setOrigin( btVector3( translates.x(),translates.y(),translates.z() ) );
	btTrans.setBasis( btRotMatrix );
	//return btTransform( btRotMatrix, btVector3( translates.x(),translates.y(),translates.z() ) );
	return btTrans;
}

//Constructor
SIM_SolverBullet::SIM_SolverBullet(const SIM_DataFactory *factory)
: BaseClass(factory), SIM_OptionsUser(this), state(0)
{
}

//Destructor
SIM_SolverBullet::~SIM_SolverBullet()
{
	if (state)
		state->removeReference();
}


//UI Param Def
const SIM_DopDescription *
SIM_SolverBullet::getSolverBulletDopDescription()
{

	static PRM_Range               simstepsRange	(PRM_RANGE_UI, 1, PRM_RANGE_UI, 100);
	static PRM_Name                simstepsName	(SIM_NAME_SIM_STEP, "Simulation Steps");
	static PRM_Default             defTen(10);
	static PRM_Default             defZero(0);
	static PRM_Default             defOne(1);


	static PRM_Template	theTemplates[] = {
		PRM_Template(PRM_FLT_J,		1, &simstepsName, &defTen, 0, &simstepsRange),
			PRM_Template()
	};

	static SIM_DopDescription   theDopDescription(true,
		"bulletrbdsolver",
		"Bullet RBD Solver",
		"Solver",
		classname(),
		theTemplates);

	return &theDopDescription;
}



// -------------------------------------------------------------------
//	@note: The solveObjectsSubclass is called once per simulation step.
//  The objects array has all of the objects you are to solve for.
SIM_Solver::SIM_Result
SIM_SolverBullet::solveObjectsSubclass(SIM_Engine &engine,
	SIM_ObjectArray &objects,
	SIM_ObjectArray &newobjects,
	SIM_ObjectArray &feedbacktoobjects,
	const SIM_Time &timestep)
{
	UT_Vector3 p, v, w;
	UT_Quaternion rot;
	std::map< int, bulletBody >::iterator bodyIt;
	btTransform btrans;
	int totalDopObjects = engine.getNumSimulationObjects();
	int totalUpdateObjects = engine.getNumSimulationObjects();
	float currTime = engine.getSimulationTime();


	if( !state )
	{
		//cerr<<"creating new state"<<endl;
		state = new SIM_SolverBulletState();
		state->addReference();
	}
	
	//cout<<"remove any that have been deleted from dops in system:"<<state->m_dynamicsWorld<<endl;
	removeDeadBodies( engine );	

	//Loop over all objects 
	for(int i = 0; i < totalDopObjects; i++)
	{
		//Get current dop sim object
		SIM_Object *currObject = (SIM_Object *)engine.getSimulationObject(i);
					
		//Check if this body has been added already, if not, add it.
		//Bullet bodies are stored in a map, the key is the dopObjectId
		bodyIt = state->m_bulletBodies->find( currObject->getObjectId() );
		
		//This is a new body (that is, it doesn't match any key in the map), make a bullet body to match
		if(bodyIt == state->m_bulletBodies->end())
			bodyIt = addBulletBody(currObject );
		
		if(bodyIt != state->m_bulletBodies->end())
		{
			processSubData(currObject, bodyIt->second);
		}	
	}

	//Run Sim and update DOPS not on init frame
	if(currTime > 0) 
	{

		SIM_SolverBullet *bulletSolverState = SIM_DATA_GET(*this, "Bullet RBD Solver", SIM_SolverBullet);
		int solverStep = 10; // default solver step
		if(bulletSolverState)
			solverStep = bulletSolverState->getSimSteps();

		state->m_dynamicsWorld->stepSimulation( timestep, solverStep );

		//Iterate over active objects and update the dop geometry	
		for (int i = 0;	i <	totalUpdateObjects; i++)
		{
		
			//Get current sim object
			SIM_Object *currObject = (SIM_Object *)engine.getSimulationObject(i);
		
			//Get State for Curr Object
			RBD_State	*rbdstate =  SIM_DATA_GET(*currObject, "Position", RBD_State);
			//SIM_BulletData *bulletstate = SIM_DATA_GET(*currObject, "Bullet Data", SIM_BulletData);
	
			//Get the matching bullet body in map
			bodyIt = state->m_bulletBodies->find( currObject->getObjectId() );
		
			if(bodyIt != state->m_bulletBodies->end())
			{	
				// Get the Bullet transform and update the DOPs subdata with it.
				(bodyIt->second.bodyId)->getMotionState()->getWorldTransform(btrans);
				// position
				UT_Vector3 p = UT_Vector3(btrans.getOrigin().getX(), btrans.getOrigin().getY(), btrans.getOrigin().getZ() );
				rbdstate->setPosition( p );
				// rotation    		
				UT_Quaternion rot = UT_Quaternion( btrans.getRotation().getX(), btrans.getRotation().getY(),
					btrans.getRotation().getZ(), btrans.getRotation().getW() );
				rbdstate->setOrientation( rot );

				// Calculate mass and inertia
				btScalar mass = 1.0f;
				if( currObject->getIsStatic() )
					mass = 0;
				else
				{
					mass = rbdstate->getMass();
					//btVector3 fallInertia(0,0,0);
					//(bodyIt->second.bodyId)->getCollisionShape()->calculateLocalInertia(mass,fallInertia);
					//cerr<<"setting mass:"<<mass<<endl;
				}

								
				if( !(bodyIt->second.bodyId)->isStaticObject() && (bodyIt->second.bodyId)->isActive() == 1 ) // if not tagged as Static
				{
					//Velocity
					btVector3 v;
					v = (bodyIt->second.bodyId)->getLinearVelocity(); 
					rbdstate->setVelocity( UT_Vector3( v.getX(), v.getY(), v.getZ() ) );
					//Angular Velocity
					btVector3 w;
					w = (bodyIt->second.bodyId)->getAngularVelocity(); 
					rbdstate->setAngularVelocity( UT_Vector3( w.getX(), w.getY(), w.getZ() ) );
				}
			}	
		}
	}

	return SIM_Solver::SIM_SOLVER_SUCCESS;
}



void
SIM_SolverBullet::generateCapsuleObject( GU_Detail 		*gdp
										,UT_DMatrix4 		xform
										,btCollisionShape 	**fallShape
										,btTransform	 	*initTransform
										//,btDefaultMotionState *fallState
										,bool autofit
										,fpreal primRadius
										,fpreal primLength
										,bool computeCom
										,UT_Vector3 *com)
{

	//	!todo:orientation
	initTransform->setIdentity();
	UT_Matrix4 	tMat;
	UT_Vector3 radii;
	tMat.identity();
	gdp->getOBBox(0,tMat,radii);
	*initTransform = convertTobtTransform((UT_DMatrix4)tMat);
	if ((computeCom) and (com!=NULL))
	{
		UT_Vector3 myCom;
		tMat.getTranslates(myCom);
		*com = myCom;
	}
	if( autofit )
	{


		
		fpreal medSize;
		int minAxis = radii.findMinAbsAxis();
		int maxAxis = radii.findMaxAbsAxis();
		if (((minAxis == 0) &&  (minAxis == 2)) || ((minAxis == 2) &&  (minAxis == 0)))
		{
			medSize = radii[1];
		}
		else 	if (((minAxis == 0) &&  (minAxis == 1)) || ((minAxis == 1) &&  (minAxis == 0)))
		{
			medSize = radii[2];
		}

		else if (((minAxis == 2) &&  (minAxis == 1)) || ((minAxis == 1) &&  (minAxis == 2)))
		{
			medSize = radii[0];
		}
		else
		{
			medSize= radii.minComponent();
		}
		
		*fallShape = new btCapsuleShape( medSize, 
						radii.maxComponent());
		
	}
	else
	{
		*fallShape = new btCapsuleShape( fabs(primRadius),fabs(primLength) );

	}
}

void 
SIM_SolverBullet::generateBoxObject(
					GU_Detail 		*gdp
					,UT_DMatrix4 		xform
					,btCollisionShape 	**fallShape
					,btTransform	 	*initTransform
					//,btDefaultMotionState *fallState
					,bool autofit
					,UT_Vector3 primSize
					,bool computeCom
					,UT_Vector3 *com)
{
	


	initTransform->setIdentity();

	UT_Matrix4 	tMat;
	UT_Vector3 scale;
	tMat.identity();
	gdp->getOBBox(0,tMat,scale);
	*initTransform = convertTobtTransform((UT_DMatrix4)tMat);	// get the btTransform from the Transform Object

	if ((computeCom) and (com!=NULL))
	{
		UT_Vector3 myCom;
		tMat.getTranslates(myCom);
		*com = myCom;
	}

	if( autofit )			// autofit turned on
	{
		//cout << "############################################# " << endl;
		//cout << currentObject->getName() << " has primitives " << gdp->primitives().entries() << endl;
		//cout << currentObject->getName() << " has points " << gdp->points().entries() << endl;
		//cout << currentObject->getName() << " bbx size  " << radii << endl;
		
		
		if (scale.minComponent() <=0.001)				// if minComponent of size is almost zero
		{
			cout<<"Bullet Solver: Warning! Bounding Box Size for simulation object is Zero on one axis:"<< scale << "  "<<endl; 	// start yelling
		}
		else
		{
			*fallShape = new btBoxShape( btVector3( scale.x(),scale.y(),scale.z() ) ); // if not, create a fallShape
		}

	}
	else
	{

		*fallShape = new btBoxShape( btVector3(primSize.x(),primSize.y(),primSize.z()) );
	}
}


void
SIM_SolverBullet::generateSphereTreeObject(	GU_Detail 		*gdp
											,UT_DMatrix4 		xform
											,btCollisionShape 	**fallShape
											,btTransform 		*initTransform
											//btDefaultMotionState *fallState,
											,uint numSpheres
											,bool computeCom
											,UT_Vector3 *com)
{

	btScalar *radii = new btScalar[numSpheres];
	btVector3 *centres = new btVector3 [numSpheres];
	btVector3 *scales	= new btVector3 [numSpheres];
	GEO_Primitive *prim;
	uint c = 0;
	UT_Vector3Array	primCenter;
	UT_FloatArray primArea;
	fpreal					primAreaSum = 0.0f;

	FOR_ALL_PRIMITIVES(gdp,prim)
	{
		if (prim->getPrimitiveId() & GEOPRIMSPHERE)
		{
			GEO_PrimSphere* sphere = dynamic_cast<GEO_PrimSphere*>(prim);
			UT_Matrix3 tMat = sphere->getTransform();
			UT_Vector3 centre = sphere->baryCenter();
			UT_Vector3 scale;
			tMat.extractScales(scale);
			primCenter.append(centre);
			primArea.append(sphere->calcArea());
			primAreaSum+=sphere->calcArea();

			radii[c] = 1;
			scales[c] = btVector3(scale.x(),scale.y(),scale.z());
			centres[c] = btVector3( centre.x(),centre.y(),centre.z() );
			//! @todo: fix this, so it is scaling the real spheres, cannot use multisphere shape for that :/
			radii[c] = scale.maxComponent();
			c++;
		}
	}
	if (numSpheres == 1)
	{
		*fallShape = new btSphereShape(radii[0]);
	}
	else
	{
		const btVector3 *centres_ptr =centres;
		const btScalar *radii_ptr = radii;
		*fallShape = new btMultiSphereShape(
					//inertiaHalfExtents,         // removed due to bullet 2.75 RC7
					centres_ptr,
					radii_ptr,
					numSpheres);
	}
	if ((computeCom) and (com!=NULL))
	{
		UT_Vector3 myCom = UT_Vector3(0,0,0);
		for (uint i=0;i<primCenter.entries();i++)
		{
				myCom +=primCenter[i] * (primArea[i]/primAreaSum);
		}
		*com = myCom;
	}
	delete radii;
	delete centres;
	delete scales;

}

void
SIM_SolverBullet::generateSphereObject(
						GU_Detail 		*gdp
						,UT_DMatrix4 		xform
						,btCollisionShape 	**fallShape
						,btTransform 		*initTransform
						//btDefaultMotionState *fallState,
						,bool autofit
						,fpreal primRadius
						,bool computeCom
						,UT_Vector3 *com)
{
	UT_BoundingSphere bSphere;
	gdp->getBSphere(&bSphere);
	initTransform->setIdentity();
	UT_Vector3 center = bSphere.getCenter();

	initTransform->setOrigin(btVector3(1,-center.y(),-center.z()));
	fpreal bRadius = bSphere.getRadius();
	fpreal useRadius = primRadius;
	if (autofit)
	{
		useRadius = bRadius;
	}
	if (com!=NULL)
	{
		if (computeCom)
		{
			*com = center;
		}
		*com *=xform;
	}

	*fallShape = new btSphereShape( useRadius );

}


void
SIM_SolverBullet::generateConcaveObject(
						GU_Detail 		*gdp
						,UT_DMatrix4 		xform
						,btCollisionShape 	**fallShape
						,btTransform	 	*initTransform
						//,btDefaultMotionState *fallState
						,bool computeCom
						,UT_Vector3 *com
						)
{

	// concave collision shape
	//cout<<"creating triangle collision shape"<<endl;
	// Transfer point positions
	initTransform->setIdentity();
	btTriangleIndexVertexArray *indexVertexArray;
	this->getTriangleIndexArray(gdp,&indexVertexArray);
	
	btGImpactMeshShape *impactShape = new btGImpactMeshShape(indexVertexArray); 
	impactShape->updateBound();
	*fallShape = impactShape;

}

void
SIM_SolverBullet::registerCollisionDispatch()
{
	btCollisionDispatcher * dispatcher = static_cast<btCollisionDispatcher *>
			(state->m_dynamicsWorld ->getDispatcher());

	btGImpactCollisionAlgorithm::registerAlgorithm(dispatcher);
}


void
SIM_SolverBullet::generateConvexHullObject(
							GU_Detail 		*gdp
							,UT_DMatrix4 		xform
							,btCollisionShape 	**fallShape 
							,btTransform		*initTransform
							//,btDefaultMotionState *fallState
							,bool computeCom
							,UT_Vector3 *com
							)
{
	btConvexHullShape *convexShape = new btConvexHullShape();  
	GEO_Point *pt;
	initTransform->setIdentity();
	FOR_ALL_GPOINTS( gdp, pt )
	{
		UT_Vector3 ppt = pt->getPos(); // * xform;
		convexShape->addPoint(btVector3(ppt.x(), ppt.y(), ppt.z() ));
	}
	*fallShape = convexShape;


}

void
SIM_SolverBullet::generateConvexTriangleObject(
							GU_Detail 	*gdp
							,UT_DMatrix4 	xform
							,btCollisionShape **fallShape
							,btTransform	*initTransform
							//,btDefaultMotionState *fallState, 	// !todo: modify fallstate
							,bool computeCom
							,UT_Vector3 *com
							)			// !todo: calculate center of mass
{
	
	initTransform->setIdentity();	
	btTriangleIndexVertexArray *indexVertexArray;
	this->getTriangleIndexArray(gdp,&indexVertexArray);
	*fallShape = new btConvexTriangleMeshShape(indexVertexArray);
}


void SIM_SolverBullet::getRBDTransform(RBD_State *rbdstate, btTransform *fallTransform)
{
	UT_Quaternion dopRot;
	UT_Vector3 dopTrans = UT_Vector3(0,0,0);
	dopRot.identity();

	if (rbdstate)
	{
		dopTrans 	= rbdstate->getPosition();
		dopRot 		= rbdstate->getOrientation();
	}
	btVector3 btDopTrans = btVector3(dopTrans[0],dopTrans[1],dopTrans[2]);
	btQuaternion btDopRot = btQuaternion(dopRot.x(),dopRot.y(),dopRot.z(),dopRot.w());

	fallTransform->setIdentity();
	fallTransform->setOrigin(btDopTrans);
	fallTransform->setRotation(btDopRot); 	// initial Rotation

}
/*
void SIM_SolverBullet::triangulatedPolyGdp(GU_Detail *gdp, GU_Detail *trigdp, GU_Detail *restGdp)
{
	trigdp = new  GU_Detail(gdp);

	

	FOR_ALL_PRIMITIVES (gdp, prim) 
	{
		GU_Detail *primGdp = new GU_Detail((GU_Primitive*)prim);
		if (prim->getPrimitiveId() & GEOPRIMPOLY)
		{
			
			if( prim->getVertexCount() > 3 )  
			{
				
			}
		}
	}
*/




void
SIM_SolverBullet::getTriangleIndexArray(GU_Detail *gdp,btTriangleIndexVertexArray **indexVertexArray)
{
		GEO_Primitive *prim;
		GEO_Point *pt;
		GEO_PrimSphere *sphere;
		int ntriangles=0, nspheres=0;
		FOR_ALL_PRIMITIVES (gdp, prim) 
		{
			if (prim->getPrimitiveId() & GEOPRIMPOLY)
			if( prim->getVertexCount() == 3 )  //add only tri's
					ntriangles++;
		}
		if( ntriangles )
		{
			btVector3 *gVertices = new btVector3[gdp->points().entries()];
			int numVertices = 0;
			int vertStride = sizeof(btVector3);
			FOR_ALL_GPOINTS( gdp, pt )
			{
				UT_Vector3 ppt = pt->getPos(); // * xform;
				gVertices[numVertices++] = btVector3( 
					ppt.x(), ppt.y(), ppt.z() );
			}
			// Transfer Indices
			int *gIndices = new int[gdp->primitives().entries()*3];
			int indexStride = 3*sizeof(int);
			int numIndices = 0;
			FOR_ALL_PRIMITIVES (gdp, prim)
				for( int i=0; i<3; i++ )
					gIndices[numIndices++] = prim->getVertex(2-i).getPt()->getNum();

			*indexVertexArray = new btTriangleIndexVertexArray(ntriangles,
								gIndices,	indexStride,
								numVertices, (btScalar*) &gVertices[0].x(), vertStride);
		}
}


void SIM_SolverBullet::calculateCOM(GU_Detail *gdp,bool calcCom, UT_Vector3 uiPivot, UT_Vector3 *com)
{
	UT_Vector3 ret_val = uiPivot;
	
	if (calcCom)
	{
		
	
		GEO_Primitive 			*prim;
		
		std::vector<UT_Vector3>		primCenter;
		std::vector<float>		primArea;
		float				primAreaSum = 0;
		

		for (int itPrim=0; itPrim <= (gdp->primitives().entries()-1);itPrim++)
		{
			////cout << "primitive ID "<< itPrim << endl;
			//cout << gdp->primitives().entry(itPrim)->getPrimitiveId() << endl;;
			if (gdp->primitives().entry(itPrim)->getPrimitiveId() & GEOPRIMPOLY)
			{
				primCenter.push_back(gdp->primitives().entry(itPrim)->baryCenter());
				////cout << "bary center" << gdp->primitives().entry(itPrim)->baryCenter()  << endl;
				primArea.push_back(gdp->primitives().entry(itPrim)->calcArea());
				primAreaSum +=gdp->primitives().entry(itPrim)->calcArea();
			}
			else
			{
				//cout << "no bary center" <<endl;
			}

			//cout << "-------------------------"<< endl;
		}
		if (primAreaSum!=0)
		{
			for (int pci=0;pci<=(primCenter.size()-1);pci++)
			{
				ret_val += primCenter[pci] * (100.0f/primAreaSum*primArea[pci]);
			}
			ret_val= ret_val/primCenter.size();
		}				
	}
	
	*com = ret_val;
}


//Adds a Bullet body to the world
//Based on type of geometry in the SIM_Object we make a Bullet body to match

std::map< int, bulletBody >::iterator
SIM_SolverBullet::addBulletBody(SIM_Object *currObject)
{
	std::map< int, bulletBody >::iterator bodyIt;
	bodyIt = state->m_bulletBodies->end();
	SIM_Geometry *myGeo;
	SIM_SopGeometry *mySopGeo;
	const SIM_SDF *sdf;
	bool useTransform = false;
	UT_Vector3 p, v, w;
	UT_Quaternion rot;

	//cout<<"adding simulation object to bullet :"<<currObject->getName()<<endl;

	// Get the Geometry...	
	myGeo = (SIM_Geometry *)currObject->getGeometry();
	mySopGeo = (SIM_SopGeometry *)currObject->getGeometry();

	useTransform = mySopGeo->getUseTransform();

	//We have geomoetry
	if(myGeo)
	{
		//Find out the type
		sdf = SIM_DATA_GETCONST(*myGeo, SIM_SDF_DATANAME, SIM_SDF);

		//useTransform = SIM_DATA_GET(*myGeo,SIM_NAME_USETRANSFORM,bool);
		bulletBody	currBody;


		RBD_State *rbdstate = NULL;
		rbdstate = SIM_DATA_GET(*currObject, "Position", RBD_State);

		SIM_BulletData *bulletstate = SIM_DATA_GET(*currObject, "Bullet Data", SIM_BulletData);
		if(rbdstate) //This is an rbd object
		{
			UT_DMatrix4 rbdPosXForm,rbdXForm,posXForm;
			SIMgetGeometryTransform(rbdPosXForm, *currObject);	//returns the absolute transform, transformed through Object Transform and SIM transformations
			SIMgetPositionTransform(rbdXForm, *currObject);	//returns the absolute transform through SIM transformations


			//were more interested in the sim transform
			// -- SIM_Transform == transformation back from Bullet into H
			// ------ OBJ-Level Transform: not baked into geometry afaik, object needs to be transformed there in bullet space
			// ----------- Center Of Mass, initial offset

			cout << currObject->getName() << " " << useTransform << "  SIMgetGeometryTransform UT DMATRIX4  " << rbdPosXForm << endl;
			cout << currObject->getName() << " " << useTransform << "  SIMgetPositionTransform UT DMATRIX4  " << rbdXForm << endl;


			UT_DMatrix4 rbdPosXFormInv = UT_DMatrix4(rbdPosXForm);
			rbdPosXFormInv.invert();
			UT_DMatrix4 rbdXFormInv = UT_DMatrix4(rbdXForm);
			rbdXFormInv.invert();
			posXForm = rbdXFormInv * rbdPosXForm;
			cout << currObject->getName() << " " << useTransform << "  RBD * inverse RBDPos UT DMATRIX4  " << posXForm << endl;

			
			// TODO: support this rbdPosXForm. Currently I don't know how to get the CompoundShape, COM and such to work.
			
			// Retrieve the gdp...
			GU_DetailHandleAutoReadLock	gdl(myGeo->getGeometry());
			GU_Detail 			*gdp = (GU_Detail *)gdl.getGdp();	

			btCollisionShape* fallShape = NULL;
			UT_Vector3 com = UT_Vector3(0.0f,0.0f,0.0f); 		// initial Center of Mass
			btTransform	initTransform;
		
			/*
			// TODO:Glue magic ;)
			UT_String glueObj;
			glueObj="";
			rbdstate->getGlueObject(glueObj);
			GU_Detail *mod_gdp = NULL;
			//cout<<"Glue Object"<< glueObj << endl;
			*/


			UT_String geoRep 	= GEO_REP_AS_IS;
			bool doConvex  		= false;
			bool doTriangulate 	= false;
			bool strict 		= false;
			if( bulletstate )
			{
				doConvex 		= bulletstate->getGeoConvex();
				doTriangulate 	= bulletstate->getGeoTri();

				bulletstate->getGeoRep(geoRep);
			}
			// by default we do not calculate the CenterOfMass
			bool computeCom	= false;
			// and it is in the origin
			UT_Vector3 userCom	= UT_Vector3(0,0,0);
			if (rbdstate)
			{
				//grab if we should calculate the COM,
				computeCom 	= rbdstate->getComputeCOM();
				// and the user suplied COM�
				userCom		= rbdstate->getPivot();
			}
			
			// copy our gdp
			GU_Detail *mod_gdp = new GU_Detail(gdp);
			// and triangulate it �
			if ((doTriangulate))
			{
				mod_gdp->convex(3);

			}
			// lets check if the gdp matches our conditions
			Gdp_Condition gdpCond;
			gdpCond.check(mod_gdp);
			// the default Center of mass is the one from the user
			com = userCom;
			if (geoRep == GEO_REP_AS_IS )	// use geometry representation as it is
			{
				if (doConvex)	// use convex hull
				{
					if (gdpCond.isConvexReady(strict)) // can it be used as a convex hull ?
					{
						this->generateConvexHullObject(
										//currObject
										//,rbdstate
										//,bulletstate
										mod_gdp
										,posXForm
										,&fallShape
										,&initTransform
										//,fallMotionState
										,computeCom
										,&com
										);
					}
					else
					{
						std::cout << " gdp cannot be used as a convex object " << std::endl;
					}
				}
				else if (!doConvex)
				{
					if (gdpCond.isConcaveReady(strict))
					{

						this->generateConcaveObject(
										//currObject
										//,rbdstate
										//,bulletstate
										mod_gdp
										,posXForm
										,&fallShape
										,&initTransform
										//,fallMotionState
										,computeCom
										,&com
										);
						this->registerCollisionDispatch();
					}
					else if (gdpCond.isSphereTreeReady(strict))
					{
						this->generateSphereTreeObject(
										//currObject
										//,rbdstate
										//,bulletstate
										gdp
										,posXForm
										,&fallShape
										,&initTransform
										//,fallMotionState
										,gdpCond.getSphereCount()
										,computeCom
										,&com);
					}
					else
					{
						std::cout << " this gdp is not able to be used as concave or sphere tree object " << std::endl;
					}
				}
			}
			else if( geoRep == GEO_REP_SPHERE )  // sphere representation
			{
				this->generateSphereObject(
								//currObject
								//,rbdstate
								//,bulletstate
								gdp
								,posXForm
								,&fallShape
								,&initTransform
								//,fallMotionState
								,bulletstate->getAutofit()
								,fabs(bulletstate->getPrimRadius())
								,computeCom
								,&com
								);
			}
			else if( geoRep == GEO_REP_BOX )  // box representation
			{
				this->generateBoxObject(
								//currObject
								//,rbdstate
								//,bulletstate
								gdp
								,posXForm
								,&fallShape
								,&initTransform
								//,fallMotionState
								,bulletstate->getAutofit()
								,bulletstate->getPrimS()
								,computeCom
								,&com
								);
				
			}
			else if( geoRep == GEO_REP_CAPSULE )  // capsule representation
			{
				this->generateCapsuleObject(
								//currObject
								//,rbdstate
								//,bulletstate
								gdp
								,posXForm
								,&fallShape
								,&initTransform
								//,fallMotionState
								,bulletstate->getAutofit()
								,bulletstate->getPrimRadius()
								,bulletstate->getPrimLength()
								,computeCom
								,&com
								);
			}

			btTransform rbdStateTransform;		//
			this->getRBDTransform(rbdstate,&rbdStateTransform);

			btTransform btComTransform;
			btComTransform.setIdentity();
			btComTransform.setOrigin(btVector3(-com.x(),-com.y(),-com.z()));


			btDefaultMotionState* fallMotionState = new btDefaultMotionState(
														rbdStateTransform,
														btComTransform);


			// now add the shapes to bullet
			if( fallShape )
			{

				btScalar mass 		= 1.0f;
				btScalar restitution 	= 0.5;
				btScalar friction 	= 0.2;
				//btScalar linear_damping = 0;
				//btScalar angular_damping = 0;	
				SIM_PhysicalParms *physicalparms = SIM_DATA_GET(*currObject,"PhysicalParms", SIM_PhysicalParms);
				if( physicalparms )
				{
					restitution = physicalparms->getBounce();
					friction = physicalparms->getFriction();
				}
	
				// Calculate mass and inertia
				if( currObject->getIsStatic() )
					mass = 0;
				else
					mass = rbdstate->getMass();

				btVector3 fallInertia(0,0,0);
				fallShape->calculateLocalInertia(mass,fallInertia);
				
				// collision margin / tolerance
				float collisionMargin = 0.0f;
				if( bulletstate )
					collisionMargin = bulletstate->getCollisionMargin();
				fallShape->setMargin( collisionMargin ); 

				btCompoundShape* compoundShape = new btCompoundShape();


				UT_Vector3 objTrans;
				rbdPosXForm.getTranslates(objTrans);
				btTransform btObj;
				btObj.setIdentity();
				btObj.setOrigin(btVector3(objTrans.x(),objTrans.y(),objTrans.z()));

				btTransform allTrans;
				allTrans.setIdentity();

				std::cout << currObject->getName() << " pre use transform "<< COUT_BTRANS_ORIGIN(allTrans) << std::endl;
				std::cout << currObject->getName() << " btObj transform "<< COUT_BTRANS_ORIGIN(btObj) << std::endl;
				if (useTransform)
				{
					//allTrans *=btObj;

				}
				std::cout << currObject->getName() <<" post use transform "<< COUT_BTRANS_ORIGIN(allTrans) << std::endl;


				//UT_Vector3 xform_t;
				//rbdPosXForm.getTranslates(xform_t);
				//btTransform btCom( btRot, btP );
				//btCom *= convertTobtTransform(rbdPosXForm);
				//btCom.setIdentity();
				//btCom.setOrigin( btVector3( xform_t.x(),xform_t.y(),xform_t.z() ) );				
				compoundShape->addChildShape( allTrans, fallShape );
				//compoundShape->addChildShape( convertTobtTransform(rbdPosXForm), fallShape );
				compoundShape->calculateLocalInertia(mass,fallInertia);
				
				btRigidBody* fallRigidBody;
				if (useTransform)
				{
					btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI = btRigidBody::btRigidBodyConstructionInfo(
													mass,
													fallMotionState,
													compoundShape,
													//fallShape,
													fallInertia);
					fallRigidBodyCI.m_friction = friction;
					fallRigidBodyCI.m_restitution = restitution;
					fallRigidBody = new btRigidBody(fallRigidBodyCI);
				}
				else
				{
					btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI = btRigidBody::btRigidBodyConstructionInfo(
													mass,
													fallMotionState,
													//compoundShape,
													fallShape,
													fallInertia);
					fallRigidBodyCI.m_friction = friction;
					fallRigidBodyCI.m_restitution = restitution;
					fallRigidBody = new btRigidBody(fallRigidBodyCI);
				}
	
				// Initialize a new body

				state->m_dynamicsWorld->addRigidBody(fallRigidBody);
				//cout<<"creating new body, id:"<<currObject->getObjectId()
				//	<<"  isStaticObject:"<<fallRigidBody->isStaticObject()<<endl;
	
				// Insert the body into the map
				currBody.bodyId	= fallRigidBody;
				currBody.isStatic =	1;
				currBody.dopId = currObject->getObjectId();
				currBody.type = "body";
				state->m_bulletBodies->insert(std::make_pair( currObject->getObjectId() , currBody	));	
				bodyIt = state->m_bulletBodies->find( currObject->getObjectId() );
			}
		}	
	}
	
	//Return the body we found
	return bodyIt;

}


//Kill off the bullet bodies that are not needed anymore
void SIM_SolverBullet::removeDeadBodies(SIM_Engine &engine)
{
	std::map< int, bulletBody >::iterator bodyIt;
	std::vector<int> deadBodies;
	
	// Accumulate all bodies to remove
	for( bodyIt = state->m_bulletBodies->begin(); bodyIt != state->m_bulletBodies->end(); ++bodyIt )
	{
		SIM_Object *currObject = (SIM_Object *)engine.getSimulationObjectFromId(bodyIt->first);
		//This object no longer exists, destroy the bullet body (add to a map)
		if(!currObject)
		{
			//cout<<"removing bullet body, id:"<<currObject->getObjectId()<<endl;
			deadBodies.push_back(bodyIt->first);
		}
	}

	// ... remove them from bullet
	for(int b = 0; b < deadBodies.size(); b++)
	{
		bodyIt	= state->m_bulletBodies->find( deadBodies[b] );
		state->m_dynamicsWorld->removeRigidBody( (bodyIt->second.bodyId) );
		delete bodyIt->second.bodyId;
		//Erase map entry
		state->m_bulletBodies->erase(bodyIt);
	}
}



//Applies forces and constraints
//This loop could include more stuff since we're 
//looping over all subData.. so may have to make it more
//general
void SIM_SolverBullet::processSubData(SIM_Object *currObject, bulletBody &body)
{	
	int f;
	UT_String forcename;
	const SIM_Data *dopData;
	
	body.bodyId->clearForces();
	
	//Loop over subdata and get forces we're interested in
	for(f = 0; f< currObject->getNumSubData(); f++)
	{
			
		//Apply Uniform Force
		if(dopData = currObject->getNthConstSubData( 
			&forcename, SIM_DataFilterByType("SIM_ForceUniform"), f, 0, SIM_DataFilterAll() ))
		{
			const SIM_ForceUniform *dopforce = SIM_DATA_CASTCONST( dopData, SIM_ForceUniform );
			UT_Vector3 thisforce = dopforce->getUniformForce();
			UT_Vector3 thistorque = dopforce->getUniformTorque();
			body.bodyId->applyCentralForce( btVector3(thisforce[0],thisforce[1],thisforce[2]) );
			//cout<<"adding central (uniform) force to id:"<<currObject->getObjectId()<<endl;
		}

		//Apply Gravity Force
		if(dopData = currObject->getNthConstSubData( 
			&forcename, SIM_DataFilterByType("SIM_ForceGravity"), f, 0, SIM_DataFilterAll() ))
		{
			const SIM_ForceGravity *dopforce = SIM_DATA_CASTCONST( dopData, SIM_ForceGravity );
			UT_Vector3 thisforce = dopforce->getGravity();
			body.bodyId->setGravity(btVector3(thisforce[0],thisforce[1],thisforce[2]));
			//cout<<"adding gravity force"<<endl;
		}

		//Apply Point Force
		if(dopData = currObject->getNthConstSubData( 
			&forcename, SIM_DataFilterByType("SIM_ForcePoint"), f, 0, SIM_DataFilterAll() ))
		{
			const SIM_ForcePoint *dopforce = SIM_DATA_CASTCONST( dopData, SIM_ForcePoint );
			UT_Vector3 thisforce = dopforce->getForce();
			UT_Vector3 thisPos = dopforce->getForcePosition();
			body.bodyId->applyForce( btVector3(thisforce[0],thisforce[1],thisforce[2]),
				btVector3(thisPos[0],thisPos[1],thisPos[2]) );
			//cout<<"adding (point) force to id:"<<currObject->getObjectId()<<endl;
		}

	}
}



void SIM_SolverBulletState::initSystem(  )
{

	//cout<<"creating world: start"<<endl;
	m_collisionConfiguration = new btDefaultCollisionConfiguration();
	m_dispatcher = new	btCollisionDispatcher( m_collisionConfiguration);
	m_broadphase = new btDbvtBroadphase();
	m_solver = new btSequentialImpulseConstraintSolver(); //new btOdeQuickstepConstraintSolver();
	btDiscreteDynamicsWorld* world = new btDiscreteDynamicsWorld( 
		m_dispatcher,
		m_broadphase,
		m_solver,
		m_collisionConfiguration);
	m_dynamicsWorld = world;
	m_dynamicsWorld->clearForces();

	m_bulletBodies = new std::map<int, bulletBody>();


	// TEMP: add groundplane
	/*
	cout<<"creating temp groundplane"<<endl;
	btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0,1,0),1);
	btDefaultMotionState* groundMotionState = new btDefaultMotionState( 
		btTransform(btQuaternion(0,0,0,1),btVector3(0,-1,0)));
	btRigidBody::btRigidBodyConstructionInfo groundRigidBodyCI(0,groundMotionState,groundShape,btVector3(0,0,0));
	btRigidBody* groundRigidBody = new btRigidBody(groundRigidBodyCI);
	m_dynamicsWorld->addRigidBody(groundRigidBody);
	*/
}

void SIM_SolverBulletState::cleanSystem(  )
{
	
	if(  m_dynamicsWorld != NULL )
	{	
		if(  refCount == 0 )
		{	
			//cout<<"destroying world: start "<< m_dynamicsWorld<<endl;
			// delete in reverse order than creation
			for( int i =  m_dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; --i ) {
				btCollisionObject* obj =  m_dynamicsWorld->getCollisionObjectArray()[i];
				btRigidBody* body = btRigidBody::upcast( obj );
				if( body && body->getMotionState() )
				delete body->getMotionState();		
				m_dynamicsWorld->removeCollisionObject( obj );
				delete obj;
			}
		
			delete  m_dynamicsWorld;
			delete  m_solver;
			delete  m_broadphase;
			delete  m_dispatcher;
			delete  m_collisionConfiguration;
			m_collisionConfiguration = NULL;
			m_dispatcher = NULL;
			m_broadphase = NULL;
			m_solver = NULL;
			m_dynamicsWorld = NULL;
			m_bulletBodies->clear();
			delete  m_bulletBodies;
			//cout<<"destroying world: end"<<endl;
		}
	}

}



void initializeSIM(void *)
{
	//register our shit with houdini
	//
	IMPLEMENT_DATAFACTORY(SIM_SolverBullet);
	IMPLEMENT_DATAFACTORY(SIM_BulletData);
}



