
#include "SIM_BulletData.h"

// ---------------------


const SIM_DopDescription*
SIM_BulletData::getBulletDataDopDescription()
{
	static PRM_Name                theGeoRep(SIM_NAME_GEO_REP, "Geometry Representation");
	static PRM_Name                theGeoTri(SIM_NAME_GEO_TRI, "Triangulate Polygons (not working yet)");
	static PRM_Name                theGeoConvex(SIM_NAME_GEO_CONVEX, "Polygons As Convex Hulls");
	static PRM_Name                thePrimT(SIM_NAME_PRIM_T, "Prim Translate");
	static PRM_Name                thePrimR(SIM_NAME_PRIM_R, "Prim Rotate");
	static PRM_Name                thePrimS(SIM_NAME_PRIM_S, "Prim Scale");
	static PRM_Name                thePrimRadius(SIM_NAME_PRIM_RADIUS, "Prim Sphere Radius");
	static PRM_Name                thePrimLength(SIM_NAME_PRIM_LENGTH, "Prim Capsule Length");
	static PRM_Name                thePrimAutofit(SIM_NAME_PRIM_AUTOFIT, "AutoFit Primitive Spheres or Boxes to Geometry");
	static PRM_Name                theCollisionMargin(SIM_NAME_COLLISION_MARGIN, "Collision Tolerance");

	static PRM_Name		theGeoRepNames[] = {
		PRM_Name(GEO_REP_AS_IS,		"As Is"),
		PRM_Name(GEO_REP_SPHERE,	"Sphere"),
		PRM_Name(GEO_REP_BOX,		"Box"),
		PRM_Name(GEO_REP_CAPSULE,	"Capsule"),
		PRM_Name(0)
	};
	static PRM_ChoiceList   theGeoRepNamesMenu((PRM_ChoiceListType)(PRM_CHOICELIST_REPLACE | PRM_CHOICELIST_EXCLUSIVE ), &(theGeoRepNames[0]) );

	static PRM_Default             defZero(0);
	static PRM_Default             defOne(1);
	static PRM_Default             defGeoRep(0, GEO_REP_AS_IS);
	static PRM_Default             defCollisionMargin(0.0);
	static PRM_Default defZeroVec[]= {PRM_Default(0), PRM_Default(0), PRM_Default(0)};
	static PRM_Default defOneVec[] = {PRM_Default(1), PRM_Default(1), PRM_Default(1)};

	static PRM_Range               geoRepRange(PRM_RANGE_UI, 0, PRM_RANGE_UI, 5);
	static PRM_Range               geoGeoTriRange(PRM_RANGE_UI, 0, PRM_RANGE_UI, 1);
	static PRM_Range               primRadiusRange(PRM_RANGE_UI, 0.1, PRM_RANGE_UI, 5);
	static PRM_Range               primLengthRange(PRM_RANGE_UI, 0.1, PRM_RANGE_UI, 5);
	static PRM_Range               collisionMarginRange(PRM_RANGE_UI, 0, PRM_RANGE_UI, 0.5);

	static PRM_Template            theTemplates[] = {
		PRM_Template(PRM_STRING,		1, &theGeoRep, &defGeoRep, &theGeoRepNamesMenu),
		PRM_Template(PRM_TOGGLE_J,	1, &theGeoTri, &defOne),
		PRM_Template(PRM_TOGGLE_J,	1, &theGeoConvex, &defZero),
		PRM_Template(PRM_XYZ,		3, &thePrimT, defZeroVec),
		PRM_Template(PRM_XYZ,		3, &thePrimR, defZeroVec),
		PRM_Template(PRM_XYZ,		3, &thePrimS, defOneVec),
		PRM_Template(PRM_FLT_J,		1, &thePrimRadius, &defOne, 0, &primRadiusRange),
		PRM_Template(PRM_FLT_J,		1, &thePrimLength, &defOne, 0, &primLengthRange),
		PRM_Template(PRM_TOGGLE_J,	1, &thePrimAutofit, &defZero, 0, &geoGeoTriRange),
		PRM_Template(PRM_FLT_J,		1, &theCollisionMargin, &defCollisionMargin, 0, &collisionMarginRange),
		PRM_Template()
	};

	static SIM_DopDescription    theDopDescription(true,
							"bulletdata",
							getName(),
							"Bullet Data",
							classname(),
							theTemplates);

	return &theDopDescription;
}

const char*
SIM_BulletData::getName()
{
	static char name[256];
	sprintf (name, "Bullet Data");
	return name;
}

SIM_BulletData::SIM_BulletData(const SIM_DataFactory *factory) : BaseClass(factory), SIM_OptionsUser(this)
{
}

SIM_BulletData::~SIM_BulletData()
{
}
