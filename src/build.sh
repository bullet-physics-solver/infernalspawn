
# this is an example file for compiling Bullet for linux.
# please go here for more info: http://odforce.net/wiki/index.php/BulletPhysicsRBDSolverDOP

#These are the default install paths for bullet under linux. Modify as you see fit.
BULLET_INC=~/local/include
BULLET_LIB=~/local/lib

rm *.o
hcustom -g -L $BULLET_LIB -I $BULLET_INC -lBulletDynamics -lBulletCollision -lBulletSoftBody -lLinearMath  SIM_SolverBullet.cpp

#If you want to manually specify the bullet library directory, use the following line (although it is highly recommended to add the Bullet lib path to your /etc/ld.so.conf so that Houdini can find it)
#hcustom -g -I $BULLET_INC -l bulletmath -l bulletdynamics -l bulletcollision -L  $BULLET_LIB  SIM_SolverBullet.cpp

