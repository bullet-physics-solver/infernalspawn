/*
 * checks if the provided gdp matches certain conditions
 *
 *  @Created on: 14.02.2010
 *  @Author: Sebastian H. Schmidt
 */

#ifndef __GDPCONDITION_H__
#define __GDPCONDITION_H__

#include <UT/UT_Matrix3.h>
#include <UT/UT_Vector3.h>
#include <GU/GU_Detail.h>
#include <GEO/GEO_PrimList.h>
#include <GEO/GEO_PrimSphere.h>

class Gdp_Condition
{
	public:
		Gdp_Condition();


		void check(GU_Detail *gdp);
		void reset();

		//! @return: true if this gdp can be used for convex fun
		//! 		 does it have at least 4 points
		bool isConvexReady(bool strict = false);

		//! @return:true if gdp can be used for concave collision
		//!					there must be at least one poly-triangle
		bool isConcaveReady(bool strict = false);

		//! @return:true if gdp is a sphereTree
		//!				at least one primitive sphere has to be present
		bool isSphereTreeReady(bool strict = false);


		uint getSphereCount();


	private:

		uint numPrimPoly;
		uint numPrimSphere;
		uint numPrimMesh;
		uint numPrimOther;
		uint numPolyTri;
		uint numPolyOther;
		uint numSphereTransform;
		uint numSphereNormal;

};


#endif /* GDPCONDITION_H_ */
