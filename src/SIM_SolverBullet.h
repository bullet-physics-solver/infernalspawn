#ifndef __SIM_SolverBullet_h__
#define __SIM_SolverBullet_h__


#include <map>
#include <vector>
#include <string>


#ifdef WIN32		// this prevents warnings when dependencies built
#include <windows.h>
#endif


#include <SIM/SIM_Solver.h>
//#include <SIM/SIM_OptionsUser.h>

#include <UT/UT_DSOVersion.h>
#include <UT/UT_XformOrder.h>
#include <PRM/PRM_Include.h>
#include <SIM/SIM_DopDescription.h>
#include <SIM/SIM_DataFilter.h>
#include <SIM/SIM_Object.h>
#include <SIM/SIM_SDF.h>
#include <SIM/SIM_ObjectArray.h>
#include <SIM/SIM_OptionsUser.h>
#include <SIM/SIM_Options.h>
#include <SIM/SIM_PhysicalParms.h>
#include <SIM/SIM_ActiveValue.h>
//#include <SIM/SIM_Data.h>
//#include <SIM/SIM_Query.h>
//#include <SIM/SIM_Utils.h>
#include <RBD/RBD_State.h>
#include <SIM/SIM_Engine.h>
#include <SIM/SIM_Force.h>
#include <SIM/SIM_ForcePoint.h>
#include <SIM/SIM_ForceUniform.h>
#include <SIM/SIM_ForceGravity.h>
//#include <SIM/SIM_Constraint.h>
//#include <SIM/SIM_ConAnchorObjSpacePos.h>
//#include <SIM/SIM_ConAnchorWorldSpacePos.h>
//#include <SIM/SIM_ConAnchor.h>
//#include <SIM/SIM_ConRel.h>
#include <SIMZ/SIM_PopGeometry.h>
#include <SIMZ/SIM_SopGeometry.h>

#include <GEO/GEO_Detail.h>
#include <GEO/GEO_PrimSphere.h>
#include <UT/UT_Quaternion.h>
#include <UT/UT_Vector3.h>
#include <UT/UT_Vector4.h>


#include <btBulletDynamicsCommon.h>
#include <BulletCollision/CollisionDispatch/btSphereSphereCollisionAlgorithm.h>
#include <BulletCollision/CollisionDispatch/btSphereTriangleCollisionAlgorithm.h>
#include <BulletCollision/Gimpact/btGImpactShape.h>
#include <BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>



#include "SIM_SolverBulletState.h"
#include "SIM_BulletData.h"
#include "Gdp_Condition.h"





#define SIM_NAME_SOURCEOBJECTS	"sourceobjects"
#define SIM_NAME_GRAVITY "gravity"
#define SIM_NAME_BOUNCE	"bounce"
#define SIM_NAME_FRICTION "friction"
#define SIM_NAME_ERP "erp"
#define SIM_NAME_CFM "cfm"
#define SIM_NAME_QUICK "quick"
#define SIM_NAME_OVERSAMPLE "oversample"
#define SIM_NAME_RAND "rand"
#define SIM_NAME_SHOWPRIM "showprim"

//class SIM_ObjectArray;
//class SIM_Geometry;
//class SIM_GeometryCopy;




#define SIM_NAME_SIM_STEP		"simstep"


//!	@note:	this is the Bullet Solver Class, inherited from a Solver and the Options for the Solver which are attached as Options
class SIM_SolverBullet : public SIM_Solver, public SIM_OptionsUser
{
	public:
		SIM_SolverBulletState       *state;
		GETSET_DATA_FUNCS_I(SIM_NAME_SIM_STEP,SimSteps);

	protected:

		//!	@note:
		virtual void makeEqualSubclass(const SIM_Data *src)
		{
			//cout<<"makeequalSubclass called, start "<<endl;
			SIM_SolverBullet *world;
			world = const_cast<SIM_SolverBullet *> SIM_DATA_CASTCONST(src, SIM_SolverBullet);
			if( world && world->state )
			{
				state = world->state;
				state->addReference();
			}
		}
		//! @note: constructor
		explicit SIM_SolverBullet(const SIM_DataFactory *factory);

		//! @note: destructor
		virtual ~SIM_SolverBullet();

		//!	@note: solves the Objects
		//!	@param:	&engine
		//! @param:	&objects
		//! @param:	&newobjects
		//! @param: &feedbacktoobjects
		//! @param	&timestep
		virtual SIM_Solver::SIM_Result solveObjectsSubclass(SIM_Engine &engine,
																SIM_ObjectArray &objects,
																SIM_ObjectArray &newobjects,
																SIM_ObjectArray &feedbacktoobjects,
																const SIM_Time &timestep);
		//!	@note:
		virtual std::map< int, bulletBody >::iterator addBulletBody(SIM_Object *currObject);

		//! @note:
		virtual void removeDeadBodies(SIM_Engine &engine);

		//! @note:
		virtual void processSubData(SIM_Object *currObject, bulletBody &body);


	private:


		//!	generates a bullet -capsule object  from a given gdp & currentstate
		//!	@param	*gdp		geometry pointer which contains the geometry which should be added to the sim
		//!	@param	*xform		transform data determined from the process of importing the geometry
		//!	@todo:	use the xform ;)
		//!	@param	*fallShape	pointer to a pointer for a collisionShape which, at the end contains the collision object
		//!	@param	*initTransform	bullet transform to describe the initial position of the dynamics object
		//! @param  autofit: if true a bounding sphere is used to determine the collision sphere, otherwise primscale is used
		//! @param  primLength: is used if autofit = false, the lengths of the capsule
		//! @param  primRadius: is used if autofit = false, the radius of the capsule
		//! @param 	computeCom: should the Center Of Mass computed
		//!	@param	*com:		pointer to center of mass
		void generateCapsuleObject(
							GU_Detail 		*gdp
							,UT_DMatrix4 		xform
							,btCollisionShape 	**fallShape
							,btTransform	 	*initTransform
							//,btDefaultMotionState *fallState,
							,bool autofit
							,fpreal primLength
							,fpreal primRadius
							,bool computeCom = false
							,UT_Vector3 *com = NULL
							);
		
		
		//!	@descr:	generates a bullet -box object  from a given gdp & currentstate
		//!	@param	*gdp		geometry pointer which contains the geometry which should be added to the sim
		//!	@param	*xform		transform data determined from the process of importing the geometry
		//!	@todo:	use the xform ;)
		//!	@param	*fallShape	pointer to a pointer for a collisionShape which, at the end contains the collision object
		//!	@param	*initTransform	bullet transform to describe the initial position of the dynamics object
		//! @param  autofit: if true a bounding sphere is used to determine the collision sphere, otherwise primscale is used
		//! @param  primScale: is used if autofit = false, the lengths of the box-sides
		//! @param 	computeCom: should the Center Of Mass computed
		//!	@param	*com:		pointer to center of mass
		void generateBoxObject(
							GU_Detail 		*gdp
							,UT_DMatrix4 		xform
							,btCollisionShape 	**fallShape
							,btTransform	 	*initTransform	
							//,btDefaultMotionState *fallState
							,bool autofit
							,UT_Vector3			primScale
							,bool computeCom = false
							,UT_Vector3 *com = NULL
							);
		
		//!	@descr:	generates a bullet -sphere object  from a given gdp & currentstate
		//!	@param	*gdp		geometry pointer which contains the geometry which should be added to the sim
		//!	@param	*xform		transform data determined from the process of importing the geometry
		//!	@todo:	use the xform ;)
		//!	@param	*fallShape	pointer to a pointer for a collisionShape which, at the end contains the collision object
		//!	@param	*initTransform	bullet transform to describe the initial position of the dynamics object
		//! @param  autofit: if true a bounding sphere is used to determine the collision sphere, otherwise primscale is used
		//! @param  primScale: is used if autofit = false, to scale the sphere
		//! @param 	computeCom: should the Center Of Mass computed
		//!	@param	*com:		pointer to center of mass
		void generateSphereObject(
							GU_Detail 		*gdp
							,UT_DMatrix4 		xform
							,btCollisionShape 	**fallShape
							,btTransform	 	*initTransform
							//,btDefaultMotionState *fallState,
							,bool autofit
							,fpreal			primScale
							,bool computeCom = false
							,UT_Vector3 *com = NULL
							);
		
		//!	@descr:	generates a concave object in bullet
		//!	@param	*gdp		geometry pointer which contains the geometry which should be added to the sim
		//!	@param	*xform		transform data determined from the process of importing the geometry
		//!	@todo:	use the xform ;)
		//!	@param	*fallShape	pointer to a pointer for a collisionShape which, at the end contains the collision object
		//!	@param	*initTransform	bullet transform to describe the initial position of the dynamics object
		//! @param 	computeCom: should the Center Of Mass computed
		//!	@param	*com:		pointer to center of mass
		void generateConcaveObject(
							GU_Detail 		*gdp
							,UT_DMatrix4 		xform
							,btCollisionShape 	**fallShape
							,btTransform	 	*initTransform 
							//,btDefaultMotionState *fallState
							,bool computeCom = false
							,UT_Vector3 *com = NULL
							);
		
		//!	@descr:	generates a bullet -convex hull object for bullet  from a given gdp & currentstate
		//!	@param	*gdp		geometry pointer which contains the geometry which should be added to the sim
		//!	@param	*xform		transform data determined from the process of importing the geometry
		//!	@todo:	use the xform ;)
		//!	@param	*fallShape	pointer to a pointer for a collisionShape which, at the end contains the collision object
		//!	@param	*initTransform	bullet transform to describe the initial position of the dynamics object
		//! @param 	computeCom: should the Center Of Mass computed
		//!	@param	*com:		pointer to center of mass
		void generateConvexHullObject(
							GU_Detail 		*gdp
							,UT_DMatrix4		 xform
							,btCollisionShape 	**fallShape
							,btTransform	 	*initTransform
							//,btDefaultMotionState *fallState,
							,bool computeCom = false
							,UT_Vector3 *com = NULL

							);


		//!	@note:	generates a bullet -convex triange object from a given gdp & currentstate
		//!	@param	*gdp		geometry pointer which contains the geometry which should be added to the sim
		//!	@param	*xform		transform data determined from the process of importing the geometry
		//!	@todo:	use the xform ;)
		//!	@param	*fallShape	pointer to a pointer for a collisionShape which, at the end contains the collision object
		//!	@param	*initTransform	bullet transform to describe the initial position of the dynamics object
		//! @param 	computeCom: should the Center Of Mass computed
		//!	@param	*com:		pointer to center of mass

		void generateConvexTriangleObject(
							GU_Detail 		*gdp
							,UT_DMatrix4 		xform
							,btCollisionShape 	**fallShape
							,btTransform	 	*initTransform
							//,btDefaultMotionState *fallState
							,bool computeCom = false
							,UT_Vector3 *com = NULL
							);

		//!	@note:	generates a bullet sphere-tree object from a given gdp & currentstate
		//!	@param	*gdp		geometry pointer which contains the geometry which should be added to the sim
		//!	@param	*xform		transform data determined from the process of importing the geometry
		//!	@todo:	use the xform ;)
		//!	@param	*fallShape:	pointer to a pointer for a collisionShape which, at the end contains the collision object
		//!	@param	*initTransform:	bullet transform to describe the initial position of the dynamics object
		//! @param  numSpheres: the number of primitives spheres in the gdp
		//! @param 	computeCom: should the Center Of Mass computed
		//!	@param	*com:		pointer to center of mass

		void generateSphereTreeObject(
							GU_Detail 		*gdp
							,UT_DMatrix4 		xform
							,btCollisionShape 	**fallShape
							,btTransform	 	*initTransform
							//,btDefaultMotionState *fallState
							,uint numSpheres
							,bool computeCom = false
							,UT_Vector3 *com = NULL
							);

		//! registers the collision dispatcher neccesary for the concave colissions
		void registerCollisionDispatch();

		//!	@note: grabs the initial translation & rotation grabbed either from the rbd-state or the bullet state
		void getRBDTransform(RBD_State *rbdstate, btTransform *initialTransform);


		//!	@note: eiter Calculates or gets the COM from the UI
		//!	@todo: center of mass is determined through the weighted barycenters
		void calculateCOM(GU_Detail *gdp,bool calcCom, UT_Vector3 uiPivot, UT_Vector3 *com);
		
		//void calculateObjTransf(UT_Vector3 com,UT_DMatrix xform,btTransform )
		//void triangulatedPolyGdp(GU_Detail *gdp, GU_Detail *trigdp, GU_Detail *restGdp = NULL);

		//!	@note: generates a TringleIndexVertex array neccessary for concave & convex objects
		void getTriangleIndexArray(GU_Detail *gdp,btTriangleIndexVertexArray **indexVertexArray);


	private:
		static const SIM_DopDescription	*getSolverBulletDopDescription(); 
	
		DECLARE_STANDARD_GETCASTTOTYPE();
		DECLARE_DATAFACTORY(SIM_SolverBullet,
			SIM_Solver,
			"Bullet Solver",
			getSolverBulletDopDescription());
};

#endif
