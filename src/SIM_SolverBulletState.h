#ifndef __SIM_SOLVERBULLETSTATE_H__
#define __SIM_SOLVERBULLETSTATE_H__


#include <map>
#include <vector>
#include <string>

#include <UT/UT_BoundingBox.h>
#include <UT/UT_String.h>


#include <btBulletDynamicsCommon.h>
#include <BulletCollision/CollisionDispatch/btSphereSphereCollisionAlgorithm.h>
#include <BulletCollision/CollisionDispatch/btSphereTriangleCollisionAlgorithm.h>
#include <BulletCollision/Gimpact/btGImpactShape.h>
#include <BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>



typedef struct bulletBodystr {
	UT_String type;
	int isStatic;
	int isSet;
	int dopId;
	btRigidBody* bodyId;
	UT_BoundingBox bbox;
} bulletBody;



class SIM_SolverBulletState {
	public:
		//! @note:
		int  refCount;

		//! @note: a map to find bullet-objects based on their Houdini $OBJID
		std::map<int, bulletBody> *m_bulletBodies;

		//! @note:
		btBroadphaseInterface*	m_broadphase;

		//! @note:
		btCollisionDispatcher*	m_dispatcher;

		//! @note:
		btConstraintSolver*	m_solver;

		//! @note:
		btDiscreteDynamicsWorld* m_dynamicsWorld;

		//! @note:
		btDefaultCollisionConfiguration* m_collisionConfiguration;

		//! @note: constructor
		SIM_SolverBulletState();

		//! @note: destructor
		~SIM_SolverBulletState();

		//! @note:
		void addReference();

		//! @note:
		void removeReference();

		//! @note:
		void initSystem( );

		//! @note:
		void cleanSystem(  );
};

#endif
