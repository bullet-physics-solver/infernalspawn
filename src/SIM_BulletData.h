
#ifndef __SIM_BulletData_h__
#define __SIM_BulletData_h__

#include <SIM/SIM_DataUtils.h>
#include <SIM/SIM_OptionsUser.h>
#include <PRM/PRM_Include.h>
#include <SIM/SIM_Data.h>
#include <SIM/SIM_DopDescription.h>


//!	@note:	 name of the geo-representation UI-Option
#define SIM_NAME_GEO_REP             "geo_representation"
//!	@note:	 name of the triangulate geometry UI-Option
#define SIM_NAME_GEO_TRI             "geo_triangulate"
//!	@note:	 name of the convex hull UI-Option
#define SIM_NAME_GEO_CONVEX          "geo_convexhull"

//!	@note:	 name of the primitive-translate UI-Option
#define SIM_NAME_PRIM_T              "prim_t"
//!	@note:	 name of the primitive-rotate UI-Option
#define SIM_NAME_PRIM_R              "prim_r"
//!	@note:	 name of the primitive-scale UI-Option
#define SIM_NAME_PRIM_S              "prim_s"

//!	@note:	 name of the primitive-radius UI-Option
#define SIM_NAME_PRIM_RADIUS         "prim_radius"

//!	@note:	 name of the primitive length UI-Option
#define SIM_NAME_PRIM_LENGTH         "prim_length"

//!	@note:	 name of the primitive-autofit UI-Option
#define SIM_NAME_PRIM_AUTOFIT        "prim_autofit"

//!	@note:	 name of the primitive-autofit-method UI-Option
#define SIM_NAME_PRIM_AUTOFIT_METHOD "prim_autofit_method"

//!	@note:	 name of the collision-margin UI-Option
#define SIM_NAME_COLLISION_MARGIN    "collision_margin"

//!	@note:	 menue entries for the - geo-representation UI-Option
#define GEO_REP_AS_IS		"as-is"
#define GEO_REP_SPHERE		"sphere"
#define GEO_REP_BOX		"box"
#define GEO_REP_CAPSULE		"capsule"


//#define GEO_REP_PITTON		"pitton"
//#define GEO_REP_SPHEREPACK	"spherepack"


//! @todo: set velocities on nodes



//! 	@note:	 Sim Bullet Dataset which is attached to each dop-object
class SIM_API SIM_BulletData : public SIM_Data,	public SIM_OptionsUser
{
	public:
		//! create function to get the information about the geometry represantation
		GETSET_DATA_FUNCS_S(SIM_NAME_GEO_REP, GeoRep);
		//! create function to get the information about the geometry trinagulation
		GETSET_DATA_FUNCS_B(SIM_NAME_GEO_TRI, GeoTri);
		//! create function to get the information about the geometry trinagulation
		GETSET_DATA_FUNCS_B(SIM_NAME_GEO_CONVEX, GeoConvex);
		//! creates function to create
		GETSET_DATA_FUNCS_V3(SIM_NAME_PRIM_T, PrimT);
		GETSET_DATA_FUNCS_V3(SIM_NAME_PRIM_R, PrimR);
		GETSET_DATA_FUNCS_V3(SIM_NAME_PRIM_S, PrimS);
		GETSET_DATA_FUNCS_F(SIM_NAME_PRIM_RADIUS, PrimRadius);
		GETSET_DATA_FUNCS_F(SIM_NAME_PRIM_LENGTH, PrimLength);
		GETSET_DATA_FUNCS_F(SIM_NAME_COLLISION_MARGIN, CollisionMargin);
		GETSET_DATA_FUNCS_B(SIM_NAME_PRIM_AUTOFIT, Autofit);
		GETSET_DATA_FUNCS_I(SIM_NAME_PRIM_AUTOFIT_METHOD, AutofitMethod);

		static const char* getName();

	protected:
		// This ensures that this data is always kept in RAM, even when
		// the cache runs out of space and writes out the simulation
		// step to disk. This is necessary because the Bullet data can't
		// be written to disk.
		virtual bool getCanBeSavedToDiskSubclass() const
			{ return false; }

	explicit             SIM_BulletData(const SIM_DataFactory *factory);
	virtual             ~SIM_BulletData();

	private:
		static const SIM_DopDescription     *getBulletDataDopDescription();

		DECLARE_STANDARD_GETCASTTOTYPE();
		DECLARE_DATAFACTORY(SIM_BulletData
				,SIM_Data
				,"Bullet Data"
				,getBulletDataDopDescription());
};

#endif
