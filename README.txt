
Open Source Bullet RBD DOP for Houdini 9.5/10.0
-----------------------------------------------

All kinds of good info will go here, I hope.

Please visit:
  http://odforce.net/wiki/index.php/BulletPhysicsRBDSolverDOP
  http://forums.odforce.net/index.php?showforum=58

This code can now be found in the git repository:
 http://gitorious.org/bullet-physics-solver/bullet-physics-solver


Building
--------
Last built successfully with bullet-2.75.
Before compiling this, you should have the Bullet library (and headers) somewhere on your system. You can configure the build script to look for it in custom paths.

1. Initialise your houdini environment
2. Run the build.sh script in the src directory.

Changes
------
0.12)
	- Code updated to compile against bullet 2.75
	- Linking issue (build.sh) fixed when compiling against static Bullet libraries.
0.11)
	- Add primitive support, along with Autofit of the Primitive
	- Instantiate the world in SolverState object so that many instances of the world can exist

0.1)
 First revision.

Contributors
------------
*) Jason Iversen, od[force].net, Rhythm&Hues
*) Van Aarde Krynauw, BlackGinger

Acknowledgements
----------------
*) James Piechota, Nafees Bin Zafar
